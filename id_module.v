`include "define_bus.v"
module id_module (
    input wire clk,rst,
    //blocking pipeline
    input wire pre_valid,next_allowin,
    output wire this_valid,this_allowin,
    //bus
    input wire[`S2_BUS-1:0] pre_bus,
    output wire[`S3_BUS-1:0] next_bus,
    //other bus
    output wire[`S1_BUS-1:0] br_bus,
    input wire[`RF_BUS-1:0] rf_bus,
    //to hazard
    input wire[`EX_HZ_BUS-1:0] ex_hz_bus,
    input wire[`OTHER_HZ_BUS-1:0] mem_hz_bus,wb_hz_bus,

    //exception
    input wire[65:0] cp0_info
);
    wire wb_eret,exception_happen;
    wire [31:0] cp0_status,cp0_cause;
    assign {wb_eret,exception_happen,cp0_status,cp0_cause}=cp0_info;
    wire stall;
    //pipeline blocking
    wire can_trans,pipeline_en;
    blocking_module blocking2(
        .clk(clk),
        .rst(rst),
        .pre_valid(pre_valid),
        .this_valid(this_valid),
        .next_allowin(next_allowin),
        .this_allowin(this_allowin),
        .can_trans(can_trans),
        .pipeline_en(pipeline_en),
        .ready_go(~stall),
        .is_exception(wb_eret|exception_happen)
    );

    //about reg and parse
    reg[`S2_BUS-1:0] pipe_reg;
    wire[31:0] inst,id_pc,if_pc;
    wire [1:0] pre_exception_situation;
    wire if_addr_error,pre_is_slot;
    assign {if_addr_error,pre_is_slot}=pre_exception_situation;
    assign {pre_exception_situation,inst,id_pc}=pipe_reg;
    assign if_pc=pre_bus[31:0];
    always @(posedge clk) begin
        if(can_trans)begin
            pipe_reg<=pre_bus;
        end
    end

    //inst
    wire[5:0] opcode,funct;
    wire[4:0] rs,rt,rd,shamt;
    wire[25:0] target;
    wire[15:0] imm;
    assign {opcode,rs,rt,rd,shamt,funct}=inst;
    assign target=inst[25:0];
    assign imm=inst[15:0];



    wire[31:0] rf_data1,rf_data2;
    //regfile
    wire[3:0] we;
    wire[4:0] waddr;
    wire[31:0] wdata;
    assign {we,waddr,wdata}=rf_bus;
    regfile regfile(
        .clk(clk),
        .raddr1(rs),
        .raddr2(rt),
        .waddr(waddr),
        .wdata(wdata),
        .we(we),
        .rdata1(rf_data1),
        .rdata2(rf_data2)
    );
    wire[`SIGNAL_CONTROL_BUS-1:0] control_bus;
    wire[11:0] alu_op;
    wire[4:0] dest;
    //to decode
    wire is_br;
    wire is_slot;
    wire[31:0] br_addr;
    wire[31:0] value1,value2;
    wire [6:0] exception_parse_bus;
    wire unreserved_inst,overflow_inst,inst_mfc0,inst_mtc0,inst_eret,
         inst_syscall,inst_break;
    assign {unreserved_inst,overflow_inst,inst_mfc0,inst_mtc0,inst_eret,
            inst_syscall,inst_break}=exception_parse_bus;
    decoder_module decoder(
       .inst(inst),
       .pc(if_pc),
       .rf_data1(value1),
       .rf_data2(value2),
       .control_bus(control_bus),
       .alu_op(alu_op),
       .dest(dest),
       .is_br(is_br),
       .is_slot(is_slot),
       .br_addr(br_addr),
       .exception_parse_bus(exception_parse_bus) 
    );
    wire br_happen=is_br&pipeline_en;
    wire br_stall;
    assign br_bus={is_slot&pipeline_en,br_stall,br_happen,br_addr};
    //exception signal
    wire interrupt=(|(cp0_cause[15:8]&cp0_status[15:8]))&~cp0_status[1]&cp0_status[0];
    wire exception_label=(if_addr_error|inst_syscall|inst_break|unreserved_inst|interrupt)&pipeline_en;
    wire [4:0] id_excode=interrupt?`INT:
                         if_addr_error?`EX_ADEL:
                         unreserved_inst?`RI:
                         inst_syscall?`Sys:
                         inst_break?`Bp:
                         5'h1f;
    wire[12:0] exception_situation={if_addr_error,overflow_inst,id_excode,exception_label,pre_is_slot,
                              inst_mfc0,inst_mtc0,inst_eret,inst_syscall};
    assign next_bus={
        exception_situation,alu_op,control_bus,dest,imm,value1,value2,id_pc
    };

    wire[31:0] ex_result,mem_result,wb_result;
    wire[1:0] sel_rdata1,sel_rdata2;
    //hazard

    data_hazard data_hazard(
        .rs(rs),
        .rt(rt),
        .stall(stall),
        .ex_hz_bus(ex_hz_bus),
        .mem_hz_bus(mem_hz_bus),
        .wb_hz_bus(wb_hz_bus),
        .sel_rdata1(sel_rdata1),
        .sel_rdata2(sel_rdata2),
        .sel_ex_result(ex_result),
        .sel_mem_result(mem_result),
        .sel_wb_result(wb_result)
    );

    assign br_stall=is_br&stall&pipeline_en;

    assign value1=(sel_rdata1==2'b00)?rf_data1:
                  (sel_rdata1==2'b01)?ex_result:
                  (sel_rdata1==2'b10)?mem_result:
                  wb_result;

    assign value2=(sel_rdata2==2'b00)?rf_data2:
                  (sel_rdata2==2'b01)?ex_result:
                  (sel_rdata2==2'b10)?mem_result:
                  wb_result;
endmodule