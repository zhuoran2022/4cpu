`define ADDR_BADV   8'b01000000
`define ADDR_COUNT  8'b01001000
`define ADDR_COMP   8'b01011000
`define ADDR_STATUS 8'b01100000
`define ADDR_CAUSE  8'b01101000
`define ADDR_EPC    8'b01110000
`define ADDR_CONFIG 8'b10000000
`include "define_bus.v"
module cp0 (
    input           clk,
    input           rst,

    //mtc0指令端口
    input           mtc0_we, //写寄存器使能
    input [7 : 0]   mtc0_addr, //要写的寄存器的地址 就是指令里的{rd,sel}
    input [31: 0]   mtc0_wdata,//写数据
    //mfc0指令端口
    input [7 : 0]   mfc0_addr, //mfc0读寄存器的地址，同mtc0_addr
    output[31: 0]   mfc0_rdata,

    //ERET指令
    input           eret_flush,

    //
    input           wb_except, //表示是否触发例外
    input           wb_bd, //表示触发例外的指令是否是延迟槽指令
    input [4 : 0]   wb_excode, //表示触发的例外类型
    input [31: 0]   wb_pc, //触发例外的指令的PC值
    input [31: 0]   wb_badvaddr, //如果触发了badvaddr指令，要记录badvaddr地址是啥

    input [5 : 0]   ex_int_in, //外部中断的端口,CPU核可以接收6个外部中断信号

    //目前实现的几个寄存器的输出,可以用于调试
    output[31: 0]   cp0_status,
    output[31: 0]   cp0_cause,
    output[31: 0]   cp0_epc,
    output[31: 0]   cp0_badvaddr,
    output[31: 0]   cp0_count,
    output[31: 0]   cp0_compare
);

/*---------------Count register----------------*/

reg tick;
reg [31 : 0] c0_count;
always @(posedge clk) begin
    if (rst)
        tick <= 1'b0;
    else 
        tick <= ~tick;
end

always @(posedge clk) begin
    if (mtc0_we && mtc0_addr == `ADDR_COUNT)
        c0_count <= mtc0_wdata;
    else if (tick)
        c0_count <= c0_count + 1'b1;
end

assign cp0_count = c0_count;
/*---------------Compare register----------------*/

reg [31 : 0] c0_compare;
always @(posedge clk) begin
    if (mtc0_we && mtc0_addr == `ADDR_COMP)
        c0_compare <= mtc0_wdata;
end

wire count_eq_compare;
assign count_eq_compare = (c0_count == c0_compare);

assign cp0_compare = c0_compare;
/*---------------Status register----------------*/

/*
31-23     22    21-16    15-8    7-2    1    0
  0       dev     0?     IM7~0    0    exl   ie
*/

//status_dev 
wire c0_status_bev;
assign c0_status_bev = 1'b1;

reg [7 : 0] c0_status_im;
always @(posedge clk) begin
    if (mtc0_we && mtc0_addr == `ADDR_STATUS)
        c0_status_im <= mtc0_wdata[15 : 8];
end

//status_exl
reg c0_status_exl;
always @(posedge clk) begin
    if (rst)
        c0_status_exl <= 1'b0;
    else if (wb_except) 
        c0_status_exl <= 1'b1;
    else if (eret_flush)
        c0_status_exl <= 1'b0;
    else if (mtc0_we && mtc0_addr == `ADDR_STATUS)
        c0_status_exl <= mtc0_wdata[1];
end    

//status_ie
reg c0_status_ie;
always @(posedge clk) begin
    if (rst)
        c0_status_ie <= 1'b0;
    else if (mtc0_we && mtc0_addr == `ADDR_STATUS)
        c0_status_ie <= mtc0_wdata[0];
end

assign cp0_status = {9'b0, 
                    c0_status_bev, 
                    6'b0, 
                    c0_status_im, 
                    6'b0, 
                    c0_status_exl, 
                    c0_status_ie};
/*---------------Cause register----------------*/

/*
31  30  29-16   15-8    7   6-2         1-0      
BD  TI    0     IP7~0   0   ExcCode      0 
*/

//cause_bd
reg c0_cause_bd;
always @(posedge clk) begin
    if (rst) 
        c0_cause_bd <= 1'b0;
    else if (wb_except && !c0_status_exl)
        c0_cause_bd <= wb_bd;
end

//cause_ti
reg c0_cause_ti;
always @(posedge clk) begin
    if (rst) 
        c0_cause_ti <= 1'b0;
    else if (mtc0_we && mtc0_addr == `ADDR_COMP)
        c0_cause_ti <= 1'b0;
    else if (count_eq_compare)
        c0_cause_ti <= 1'b1;
end

//cause_ip
reg [7 : 0] c0_cause_ip;
always @(posedge clk) begin
    if (rst)
        c0_cause_ip[7 : 2] <= 6'b0;
    else begin
        c0_cause_ip[7] <= ex_int_in[5] | c0_cause_ti;
        c0_cause_ip[6 : 2] <= ex_int_in[4 : 0];
    end
end

always @(posedge clk) begin
    if (rst)    
        c0_cause_ip[1 : 0] <= 2'b0;
    else if (mtc0_we && mtc0_addr == `ADDR_CAUSE)
        c0_cause_ip[1 : 0] <= mtc0_wdata[9 : 8];
end

//cause_excode
reg [4 : 0] c0_cause_excode;
always @(posedge clk) begin
    if (rst)
        c0_cause_excode <= 5'b0;
    else if (wb_except)
        c0_cause_excode <= wb_excode;
end

assign cp0_cause = {c0_cause_bd,
                    c0_cause_ti,
                    14'b0,
                    c0_cause_ip,
                    1'b0,
                    c0_cause_excode,
                    2'b0};
/*---------------EPC register----------------*/

reg [31 : 0] c0_epc;
always @(posedge clk) begin
    if (wb_except && !c0_status_exl)
        c0_epc <= wb_bd ? wb_pc - 4 : wb_pc; 
    else if (mtc0_we && mtc0_addr == `ADDR_EPC)
        c0_epc <= mtc0_wdata;
end

assign cp0_epc = c0_epc;
/*---------------BadVAddr register----------------*/

reg [31 : 0] c0_badvaddr;
always @(posedge clk) begin
    if (wb_except && (wb_excode == `EX_ADEL || wb_excode == `EX_ADES))
        c0_badvaddr <= wb_badvaddr;
end

assign cp0_badvaddr = c0_badvaddr;

//MFC0 READ
assign mfc0_rdata = ({32{mfc0_addr == `ADDR_BADV}} & cp0_badvaddr) | ({32{mfc0_addr == `ADDR_COUNT}}  & cp0_count) |
                    ({32{mfc0_addr == `ADDR_COMP}} & cp0_compare)  | ({32{mfc0_addr == `ADDR_STATUS}} & cp0_status)|
                    ({32{mfc0_addr == `ADDR_CAUSE}}& cp0_cause)    | ({32{mfc0_addr == `ADDR_EPC}}    & cp0_epc);

endmodule