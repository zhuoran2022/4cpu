//00 寄存器 01 exe 02 mem 03 wb
//ex传过来40位，mem传过来39，wb传过来39
`include "define_bus.v"
module data_hazard (
    input [4:0] rs,rt,
    //load use hazard
    output stall,
    input[`EX_HZ_BUS-1:0] ex_hz_bus,
    input[`OTHER_HZ_BUS-1:0] mem_hz_bus,wb_hz_bus,
    output wire[1:0] sel_rdata1,sel_rdata2,
    output wire[31:0] sel_ex_result,sel_mem_result,sel_wb_result
);

    //parse
    //exe
    wire mem_read,ex_pipeline_en,ex_reg_write,ex_inst_mfc0;
    wire [4:0] ex_dest;
    wire[31:0] ex_result;
    assign {ex_inst_mfc0,mem_read,ex_pipeline_en,ex_reg_write,ex_dest,ex_result}=ex_hz_bus;
    //mem
    wire mem_pipeline_en,mem_reg_write,mem_inst_mfc0;
    wire [4:0] mem_dest;
    wire[31:0] mem_result;
    assign {mem_inst_mfc0,mem_pipeline_en,mem_reg_write,mem_dest,mem_result}=mem_hz_bus;
    //wb
    wire wb_pipeline_en,wb_reg_write,wb_inst_mfc0;
    wire [4:0] wb_dest;
    wire[31:0] wb_result;
    assign {wb_inst_mfc0,wb_pipeline_en,wb_reg_write,wb_dest,wb_result}=wb_hz_bus;
    //立即数的不用管，ex阶段会选择立即数的
    wire ex_flag=ex_pipeline_en&ex_reg_write&~(ex_dest==5'b0);
    wire mem_flag=mem_pipeline_en&mem_reg_write&~(mem_dest==5'b0);
    wire wb_flag=wb_pipeline_en&wb_reg_write&~(wb_dest==5'b0);

    wire ex1=(rs==ex_dest);
    wire ex2=(rt==ex_dest);
    wire mem1=(rs==mem_dest);
    wire mem2=(rt==mem_dest);
    wire wb1=(rs==wb_dest);
    wire wb2=(rt==wb_dest);


    wire left_ex_happen=ex_flag&ex1;
    wire right_ex_happen=ex_flag&ex2;
    wire left_mem_happen=mem_flag&mem1;
    wire right_mem_happen=mem_flag&mem2;
    wire left_wb_happen=wb_flag&wb1;
    wire right_wb_happen=wb_flag&wb2;


    assign sel_rdata1=left_ex_happen?2'b01:
                      left_mem_happen?2'b10:
                      left_wb_happen?2'b11:
                      2'b00;

    assign sel_rdata2=right_ex_happen?2'b01:
                      right_mem_happen?2'b10:
                      right_wb_happen?2'b11:
                      2'b00;

    assign sel_ex_result=ex_result;
    assign sel_mem_result=mem_result;
    assign sel_wb_result=wb_result;

    //取数使用阻塞
    assign stall=(left_ex_happen|right_ex_happen)&mem_read|
                 (left_ex_happen|right_ex_happen)&ex_inst_mfc0|
                 (left_mem_happen|right_mem_happen)&mem_inst_mfc0|
                 (left_wb_happen|right_wb_happen)&wb_inst_mfc0;
endmodule