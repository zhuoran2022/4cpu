`include "define_bus.v"
module exe_module (
    input wire clk,rst,
    //blocking pipeline
    input wire pre_valid,next_allowin,
    output wire this_valid,this_allowin,
    //bus
    input wire [`S3_BUS-1:0] pre_bus,
    output wire [`S4_BUS-1:0] next_bus,
    //data ram
    output wire data_ram_en,
    output wire[3:0] data_ram_wen,
    output wire[31:0] data_ram_addr,
    output wire[31:0] data_ram_wdata,
    //hazard
    output wire[`EX_HZ_BUS-1:0] ex_hz_bus,
    //exception
    input wire[1:0] exception_wb_info,
    input wire[1:0] exception_mem_info
);
    wire wb_exception_happen,wb_eret;
    wire mem_exception_happen,mem_eret;
    wire exe_exception_label;
    assign {wb_eret,wb_exception_happen}=exception_wb_info;
    assign {mem_eret,mem_exception_happen}=exception_mem_info;

    wire exception_stall=wb_exception_happen|mem_exception_happen|
                         wb_eret|mem_eret|exe_exception_label;
    //pipeline blocking
    wire can_trans,pipeline_en,ready_go;
    blocking_module blocking3(
        .clk(clk),
        .rst(rst),
        .pre_valid(pre_valid),
        .this_valid(this_valid),
        .next_allowin(next_allowin),
        .this_allowin(this_allowin),
        .can_trans(can_trans),
        .pipeline_en(pipeline_en),
        .ready_go(ready_go),
        .is_exception(wb_eret|wb_exception_happen)
    );
    //about reg
    reg[`S3_BUS-1:0] pipe_reg;
    always @(posedge clk) begin
        if(can_trans)begin
            pipe_reg<=pre_bus;
        end
    end
    //parse pre bus
    wire[11:0] alu_op;
    wire[`SIGNAL_CONTROL_BUS-1:0] control_bus;
    wire[4:0] dest;
    wire[15:0] imm;
    wire[31:0] rf_data1,rf_data2,pc;
    wire [12:0] pre_exception_situation;
    //parse exception
    wire if_addr_error,overflow_inst,id_exception_label,is_slot;
    wire [4:0]id_excode;
    wire inst_mfc0,inst_mtc0,inst_eret,inst_syscall;
    assign {if_addr_error,overflow_inst,id_excode,id_exception_label,is_slot,
            inst_mfc0,inst_mtc0,inst_eret,inst_syscall}=pre_exception_situation;
    assign {pre_exception_situation,
            alu_op,
            control_bus,
            dest,imm,
            rf_data1,
            rf_data2,
            pc}=pipe_reg;
    wire op1_id_shamt,op1_is_pc,op2_is_imm,op2_is_8,
                        mem_to_reg,mem_we,reg_write,
                        op2_imm_zero_ext;
    wire [3:0] mul_div_bus,hi_lo_control_bus,load_control_bus;
    wire [3:0] store_control_bus;
    //signal mul and div
    wire inst_mult,inst_multu,inst_div,inst_divu;
    //signal hilo
    wire inst_mfhi,inst_mthi,inst_mflo,inst_mtlo;
    assign {inst_mult,inst_multu,inst_div,inst_divu}=mul_div_bus;
    assign {inst_mthi,inst_mtlo,inst_mfhi,inst_mflo}=hi_lo_control_bus;
    //lb lbu lh lhu
    wire inst_lb,inst_lbu,inst_lh,inst_lhu;
    assign {inst_lb,inst_lbu,inst_lh,inst_lhu}=load_control_bus;
    //sb sh swl swr
    wire inst_sb,inst_sh,inst_swl,inst_swr;
    assign {inst_sb,inst_sh,inst_swl,inst_swr}=store_control_bus;
    //lwl lwr
    wire inst_lwl,inst_lwr;
    wire [1:0] unalignment_load;
    assign {inst_lwl,inst_lwr}=unalignment_load;
    assign {op1_id_shamt,op1_is_pc,op2_imm_zero_ext,op2_is_imm,op2_is_8,
                        mem_to_reg,mem_we,reg_write,
                        mul_div_bus,
                        hi_lo_control_bus,
                        load_control_bus,
                        store_control_bus,
                        unalignment_load
                        }=control_bus;
    
    wire[31:0] op1,op2,alu_result;
    assign op1=op1_id_shamt?{27'b0,imm[10:6]}:op1_is_pc?pc:rf_data1;
    wire imm_signal=op2_imm_zero_ext?1'b0:imm[15];
    assign op2=op2_is_imm?{{16{imm_signal}},imm[15:0]}:op2_is_8?32'd8:rf_data2;
    
    wire overflow;
    alu_module alu(
        .alu_op(alu_op),
        .alu_src1(op1),
        .alu_src2(op2),
        .alu_result(alu_result),
        .overflow(overflow)
    );

    //hilo reg
    reg[31:0] hi_reg,lo_reg;
    wire [63:0] div_result,divu_result,mult_result;
    always @(posedge clk) begin
        if(inst_mthi&~exception_stall&pipeline_en)begin
            hi_reg<=rf_data1;
        end
        else if((inst_mult|inst_multu)&~exception_stall&pipeline_en)begin
            hi_reg<=mult_result[63:32];
        end
        else if(inst_div&~exception_stall&pipeline_en)begin
            hi_reg<=div_result[31:0];
        end
        else if(inst_divu&~exception_stall&pipeline_en)begin
            hi_reg<=divu_result[31:0];
        end

        if(inst_mtlo&~exception_stall&pipeline_en)begin
            lo_reg<=rf_data1;
        end
        else if((inst_mult|inst_multu)&~exception_stall&pipeline_en)begin
            lo_reg<=mult_result[31:0];
        end
        else if(inst_div&~exception_stall&pipeline_en)begin
            lo_reg<=div_result[63:32];
        end
        else if(inst_divu&~exception_stall&pipeline_en)begin
            lo_reg<=divu_result[63:32];
        end
    end

    //mult multu
    mult_module mult_module(
        .op1(rf_data1),
        .op2(rf_data2),
        .inst_mult(inst_mult),
        .inst_multu(inst_multu),
        .mult_result(mult_result)
    );

    //div divu
    //除法有时间的话试着自己写写，先用ip核
    wire unsigned_dividend_tready,unsigned_dividend_tvalid;
    wire unsigned_divisor_tready,unsigned_divisor_tvalid;
    wire signed_dividend_tready,signed_dividend_tvalid;
    wire signed_divisor_tready,signed_divisor_tvalid;
    wire unsigned_dout_tvalid,signed_dout_tvalid;
    unsigned_divider unsigned_divider (
        .aclk                   (clk),
        .s_axis_dividend_tdata  (rf_data1),
        .s_axis_dividend_tready (unsigned_dividend_tready),
        .s_axis_dividend_tvalid (unsigned_dividend_tvalid),
        .s_axis_divisor_tdata   (rf_data2),
        .s_axis_divisor_tready  (unsigned_divisor_tready),
        .s_axis_divisor_tvalid  (unsigned_divisor_tvalid),
        .m_axis_dout_tdata      (divu_result),
        .m_axis_dout_tvalid     (unsigned_dout_tvalid)
    );

    signed_divider signed_divider (
        .aclk                   (clk),
        .s_axis_dividend_tdata  (rf_data1),
        .s_axis_dividend_tready (signed_dividend_tready),
        .s_axis_dividend_tvalid (signed_dividend_tvalid),
        .s_axis_divisor_tdata   (rf_data2),
        .s_axis_divisor_tready  (signed_divisor_tready),
        .s_axis_divisor_tvalid  (signed_divisor_tvalid),
        .m_axis_dout_tdata      (div_result),
        .m_axis_dout_tvalid     (signed_dout_tvalid)
    );
    //dividend_send,divisor_send
    reg[1:0] unsigned_div_regs,signed_div_regs;
    assign ready_go=pipeline_en&inst_div&~wb_eret&~wb_exception_happen?signed_dout_tvalid:
                    pipeline_en&inst_divu&~wb_eret&~wb_exception_happen?unsigned_dout_tvalid:
                                                            1'b1;
    //assign ready_go=1'b1;
    //divu
    assign unsigned_dividend_tvalid=pipeline_en&inst_divu&~unsigned_div_regs[1];
    assign unsigned_divisor_tvalid=pipeline_en&inst_divu&~unsigned_div_regs[0];
    //div
    assign signed_dividend_tvalid=pipeline_en&inst_div&~signed_div_regs[1];
    assign signed_divisor_tvalid=pipeline_en&inst_div&~signed_div_regs[0];
    
    always @(posedge clk) begin
        if(rst)begin
            unsigned_div_regs[1]<=1'b0;
        end
        else if(unsigned_dividend_tready&unsigned_dividend_tvalid)begin
            unsigned_div_regs[1]<=1'b1;
        end
        else if(unsigned_dout_tvalid)begin
            unsigned_div_regs[1]<=1'b0;
        end

        if(rst)begin
            unsigned_div_regs[0]<=1'b0;
        end
        else if(unsigned_divisor_tready&unsigned_divisor_tvalid)begin
            unsigned_div_regs[0]<=1'b1;
        end
        else if(unsigned_dout_tvalid)begin
            unsigned_div_regs[0]<=1'b0;
        end
    end
    always @(posedge clk) begin
        if(rst)begin
            signed_div_regs[1]<=1'b0;
        end
        else if(signed_dividend_tready&signed_dividend_tvalid)begin
            signed_div_regs[1]<=1'b1;
        end
        else if(signed_dout_tvalid)begin
            signed_div_regs[1]<=1'b0;
        end

        if(rst)begin
            signed_div_regs[0]<=1'b0;
        end
        else if(signed_divisor_tready&signed_divisor_tvalid)begin
            signed_div_regs[0]<=1'b1;
        end
        else if(signed_dout_tvalid)begin
            signed_div_regs[0]<=1'b0;
        end
    end

    //data ram
    //about store inst
    wire [1:0] store_addr_bit=alu_result[1:0];
    wire [31:0] swl_data=(store_addr_bit==2'b00)?{24'b0,rf_data2[31:24]}:
                         (store_addr_bit==2'b01)?{16'b0,rf_data2[31:16]}:
                         (store_addr_bit==2'b10)?{8'b0,rf_data2[31:8]}:
                                                rf_data2;
    wire [31:0] swr_data=(store_addr_bit==2'b00)?rf_data2:
                         (store_addr_bit==2'b01)?{rf_data2[23:0],8'b0}:
                         (store_addr_bit==2'b10)?{rf_data2[15:0],16'b0}:
                                                 {rf_data2[7:0],24'b0};

    wire [31:0] st_data=inst_sb?{4{rf_data2[7:0]}}:
                        inst_sh?{2{rf_data2[15:0]}}:
                        inst_swl?swl_data:
                        inst_swr?swr_data:
                                 rf_data2;
    wire [3:0] sb_wen_bit=(store_addr_bit==2'b00)?4'b0001:
                          (store_addr_bit==2'b01)?4'b0010:
                          (store_addr_bit==2'b10)?4'b0100:
                                                  4'b1000;
    wire [3:0] sh_wen_bit=(store_addr_bit==2'b00)?4'b0011:
                                                  4'b1100;
    wire [3:0] swl_wen_bit=(store_addr_bit==2'b00)?4'b0001:
                           (store_addr_bit==2'b01)?4'b0011:
                           (store_addr_bit==2'b10)?4'b0111:
                                                   4'b1111;
    wire [3:0] swr_wen_bit=(store_addr_bit==2'b00)?4'b1111:
                           (store_addr_bit==2'b01)?4'b1110:
                           (store_addr_bit==2'b10)?4'b1100:
                                                   4'b1000;
    wire [3:0] final_wen_bit=inst_sb?sb_wen_bit:
                             inst_sh?sh_wen_bit:
                             inst_swl?swl_wen_bit:
                             inst_swr?swr_wen_bit:
                             4'hf;

    assign data_ram_en=1'b1;
    assign data_ram_wen=mem_we&pipeline_en&~exception_stall?final_wen_bit:4'h0;
    assign data_ram_wdata=st_data;
    assign data_ram_addr=alu_result;

    //mfhi mflo
    wire [31:0] final_result=inst_mtc0?rf_data2:
                             inst_mfhi?hi_reg:
                             inst_mflo?lo_reg:
                             alu_result;
    //next exception
    wire overflow_happen=overflow&overflow_inst;
    wire load_addr_happen=(mem_to_reg&~(inst_lb|inst_lbu|inst_lh|inst_lhu|inst_lwl|inst_lwr)&
                           alu_result[1:0]!=2'b00)|((inst_lhu|inst_lh)&alu_result[0]);
    wire store_addr_happen=(mem_we&final_wen_bit==4'hf&~(inst_swl|inst_swr)&(|store_addr_bit))|
                           (inst_sh&store_addr_bit[0]);
    wire[31:0] badvaddr=if_addr_error?pc:alu_result;
    wire[7:0] cp0_dest={imm[15:11],imm[2:0]};
    wire[4:0] exe_excode=if_addr_error?id_excode:
                overflow_happen?`Ov:
                load_addr_happen?`EX_ADEL:
                store_addr_happen?`EX_ADES:
                id_excode;
    assign exe_exception_label=(overflow_happen|id_exception_label|load_addr_happen|
                              store_addr_happen)&pipeline_en;
    wire[50:0] exception_situation={exe_exception_label,exe_excode,badvaddr,
                              cp0_dest,is_slot,inst_mfc0,
                              inst_mtc0,inst_eret,inst_syscall};
    // output wire data_ram_en,
    // output wire[3:0] data_ram_wen,
    // output wire[31:0] data_ram_addr,
    // output wire[31:0] data_ram_wdata,
    wire [36:0] cache_need_bus={data_ram_en,data_ram_wen,data_ram_wdata};
    assign next_bus={
        cache_need_bus,exception_situation,unalignment_load,load_control_bus,mem_to_reg,reg_write,dest,final_result,pc
    };

    assign ex_hz_bus={inst_mfc0,mem_to_reg,pipeline_en&~wb_eret&~wb_exception_happen,reg_write,dest,final_result};
endmodule