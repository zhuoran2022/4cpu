module blocking_module (
    input wire clk,rst,
    input wire pre_valid,next_allowin,
    output wire this_valid,this_allowin,
    output wire can_trans,
    input wire ready_go,
    output reg pipeline_en,
    input wire is_exception
);
    //wire ready_go=1'b1;
    assign this_allowin=~pipeline_en|ready_go&next_allowin;
    assign this_valid=pipeline_en&ready_go&~is_exception;
    assign can_trans=this_allowin&pre_valid;
    always @(posedge clk) begin
        if(rst)begin
            pipeline_en<=1'b0;
        end
        else if(this_allowin)begin
            pipeline_en<=pre_valid;
        end
    end  
endmodule