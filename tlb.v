`timescale 1ns / 1ps

`define ENTRY_LENGTH 78
`define ITEM_LENGTH 25

`define VPN2 77:59
`define ASID 58:51
`define G 50
`define ITEM0 49:25
`define PFN0 49:30
`define C0 29:27
`define D0 26
`define V0 25
`define ITEM1 24:0
`define PFN1 24:5
`define C1 4:2
`define D1 1
`define V1 0   

module tlb
#(
    parameter TLBNUM = 16
)
(
    input clk,
    input rst,

    input [                 2 : 0] config_k0, //连接CP0 config寄存器的k0域
    //查找端口0，可以用在阶段阶段
    input [                18 : 0] s0_vpn2, //虚拟地址前19位
    input                          s0_odd_page, //虚拟地址第20位
    input [                 7 : 0] s0_asid, //连接CP0 EnrtyHi寄存器的ASID域
    output                         s0_found, //表示是否找到TLB表项
    output[$clog2(TLBNUM) - 1 : 0] s0_index, //找到的TLB表项的序号
    output[                19 : 0] s0_pfn, //虚拟页号对应的物理页号
    output[                 2 : 0] s0_c, //该页的缓存属性
    output                         s0_d, //该页是否是dirty
    output                         s0_v, //该页是否是valid
    output                         s0_cache_invalid, //物理地址是否走缓存
    output                         s0_go_through_tlb, //地址映射方式，1表示tlb映射，0表示直接映射
    //查找端口1，可以用在访存阶段，同上
    input [                18 : 0] s1_vpn2,
    input                          s1_odd_page,
    input [                 7 : 0] s1_asid,
    output                         s1_found,
    output[$clog2(TLBNUM) - 1 : 0] s1_index,
    output[                19 : 0] s1_pfn,
    output[                 2 : 0] s1_c,
    output                         s1_d,
    output                         s1_v,
    output                         s1_cache_invalid,
    output                         s1_go_through_tlb,
    //写端口，用于实现TLB指令
    input                          we, //写使能
    input [$clog2(TLBNUM) - 1 : 0] w_index, //写tlb表项的序号
    input [`ENTRY_LENGTH - 1  : 0] w_entry, //表项内容
    //读端口，用于实现TLB指令
    input [$clog2(TLBNUM) - 1 : 0] r_index, //读tlb表项的序号
    output[`ENTRY_LENGTH - 1  : 0] r_entry //表项内容
);

    localparam LOG2_TLBNUM = $clog2(TLBNUM);

    reg [ENTRY_LENGTH - 1 : 0] tlb [TLBNUM - 1 : 0];

    /*-------------------------search-----------------------*/
    wire [TLBNUM - 1 : 0] match0; //对应表项是否匹配
    wire [TLBNUM - 1 : 0] match1;
    
    genvar i;
    generate
        for (i = 0; i < TLBNUM; i = i + 1)
        begin
            assign match0[i] = (tlb[i][`VPN2] == s0_vpn2) && (tlb[i][`G] || tlb[i][`ASID] == s0_asid);
            assign match1[i] = (tlb[i][`VPN2] == s1_vpn2) && (tlb[i][`G] || tlb[i][`ASID] == s1_asid);
        end
    endgenerate

    assign s0_found = (|match0) ? 1'b1 : 1'b0;
    assign s1_found = (|match1) ? 1'b1 : 1'b0;

    wire [LOG2_TLBNUM - 1  : 0] s_index[1 : 0][TLBNUM - 1 : 0];
    generate
        for (i = 0; i < TLBNUM; i = i + 1) begin
            if (i == 0) begin
                assign s_index[0][0] = i & {LOG2_TLBNUM{match0[i]}};
                assign s_index[1][0] = i & {LOG2_TLBNUM{match1[i]}};
            end
            else begin
                assign s_index[0][i] = (i & {LOG2_TLBNUM{match0[i]}}) | s_index[0][i - 1];
                assign s_index[1][i] = (i & {LOG2_TLBNUM{match1[i]}}) | s_index[1][i - 1];
            end
        end
    endgenerate

    assign s0_index = s_index[0][TLBNUM - 1];
    assign s1_index = s_index[1][TLBNUM - 1];

    wire [ITEM_LENGTH - 1 : 0] s0_item;
    wire [ITEM_LENGTH - 1 : 0] s1_item;
    assign s0_item = s0_odd_page == 1'b0 ? tlb[s0_index][`ITEM0] : tlb[s0_index][`ITEM1];
    assign s1_item = s1_odd_page == 1'b0 ? tlb[s1_index][`ITEM0] : tlb[s1_index][`ITEM1];

    assign {s0_c, s0_d, s0_v} = s0_item[4 : 0];
    assign {s1_c, s1_d, s1_v} = s1_item[4 : 0];

    wire s0_kesg0, s0_kesg1;
    assign s0_kesg0 = s0_vpn2[19 : 16] == 4'd8 || s0_vpn2[19 : 16] == 4'd9;
    assign s0_kesg1 = s0_vpn2[19 : 16] == 4'd10|| s0_vpn2[19 : 16] == 4'd11;
    assign s0_cache_invalid = (s0_c != 3'd3) || s0_kesg1 || (s0_kesg0 && config_k0 != 3'd3);
    assign s0_go_through_tlb = ~(s0_kesg0 && s0_kesg1);

    wire s1_kesg0, s1_kesg1;
    assign s1_kesg0 = s1_vpn2[19 : 16] == 4'd8 || s1_vpn2[19 : 16] == 4'd9;
    assign s1_kesg1 = s1_vpn2[19 : 16] == 4'd10|| s1_vpn2[19 : 16] == 4'd11;
    assign s1_cache_invalid = (s1_c != 3'd3) || s1_kesg1 || (s1_kesg0 && config_k0 != 3'd3);
    assign s1_go_through_tlb = ~(s1_kesg0 && s1_kesg1);

    assign s0_pfn = (s0_kesg0 || s0_kesg1) ? s0_item[`PFN1] & 32'b0001_1111_1111_1111 : s0_item[`PFN1];
    assign s1_pfn = (s1_kesg0 || s1_kesg1) ? s1_item[`PFN1] & 32'b0001_1111_1111_1111 : s1_item[`PFN1];

    /*-------------------------write-----------------------*/
    integer j;
    always @(posedge clk) begin
        if (rst) begin
            for (j = 0; j < TLBNUM; j = j + 1) begin
                tlb[j] <= 0;
            end
        end
        else if (we) begin
            tlb[w_index] <= w_entry;
        end
    end

    /*-------------------------read-----------------------*/
    assign r_entry = tlb[r_index];

endmodule
