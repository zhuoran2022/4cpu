`include "define_bus.v"
module wb_module (
    input wire clk,rst,
    //blocking pipeline
    input wire pre_valid,
    output wire this_allowin,
    //bus
    input wire[`S5_BUS-1:0] pre_bus,
    output wire[`RF_BUS-1:0] rf_bus,
    //debug
    output wire[31:0] debug_wb_pc,
    output wire[3:0] debug_wb_rf_wen,
    output wire[4:0] debug_wb_rf_wnum,
    output wire[31:0] debug_wb_rf_wdata,
    //hazard
    output wire[`OTHER_HZ_BUS-1:0] wb_hz_bus,
    //except to if
    output wire[33:0] exception_if_bus,
    //except to id
    output wire[65:0] exception_id_bus,
    output wire [1:0] exception_wb_bus,
    input wire [5:0] ext_int
);
    //pipeline blocking
    wire can_trans,pipeline_en,this_valid;
    blocking_module blocking5(
        .clk(clk),
        .rst(rst),
        .pre_valid(pre_valid),
        .this_valid(this_valid),
        .next_allowin(1'b1),
        .this_allowin(this_allowin),
        .can_trans(can_trans),
        .pipeline_en(pipeline_en),
        .ready_go(1'b1),
        .is_exception(1'b0)
    );
    //reg and parse
    reg[`S5_BUS-1:0] pipe_reg;
    wire[3:0] final_wen_bit;
    wire[4:0] dest;
    wire[31:0] result,pc;
    wire [50:0] pre_exception_situation;
    assign {pre_exception_situation,final_wen_bit,dest,result,pc}=pipe_reg;
    always @(posedge clk) begin
        if(can_trans)begin
            pipe_reg<=pre_bus;
        end
    end

    wire [31:0] final_result;
    //rf_bus
    wire[3:0] rf_we=final_wen_bit&{4{pipeline_en&~pre_exception_label}};
    assign rf_bus={
        rf_we,dest,final_result
    };
    //parse exception
    wire pre_exception_label,is_slot,inst_mfc0,inst_mtc0,inst_eret,inst_syscall;
    wire [4:0] excode;
    wire [31:0] badvaddr;
    wire [7:0] cp0_dest;
    assign {pre_exception_label,
            excode,badvaddr,
            cp0_dest,is_slot,inst_mfc0,
            inst_mtc0,inst_eret,inst_syscall}=pre_exception_situation;
    wire[31:0] cp0_status,cp0_cause,cp0_epc,cp0_badvaddr,cp0_count,cp0_compare;
    wire[31:0] mfc0_rdata;
    cp0 cp0(
        .clk(clk),
        .rst(rst),
        .wb_except(pre_exception_label),
        .mtc0_we(inst_mtc0&pipeline_en&~pre_exception_label),
        .mtc0_addr(cp0_dest),
        .mtc0_wdata(result),
        .mfc0_addr(cp0_dest),
        .mfc0_rdata(mfc0_rdata),
        .eret_flush(inst_eret&pipeline_en),
        .wb_bd(is_slot),
        .wb_excode(excode),
        .wb_pc(pc),
        .wb_badvaddr(badvaddr),
        .ex_int_in(ext_int),
        //reg
        .cp0_status(cp0_status),
        .cp0_cause(cp0_cause),
        .cp0_epc(cp0_epc),
        .cp0_badvaddr(cp0_badvaddr),
        .cp0_count(cp0_count),
        .cp0_compare(cp0_compare)
    );
    assign debug_wb_pc=pc;
    assign debug_wb_rf_wen=rf_we;
    assign debug_wb_rf_wnum=dest;
    assign debug_wb_rf_wdata=final_result;

    assign final_result=inst_mfc0?mfc0_rdata:
                        result;

    assign wb_hz_bus={inst_mfc0,pipeline_en,|rf_we,dest,final_result};

    assign exception_if_bus={inst_eret&pipeline_en,pre_exception_label&pipeline_en,cp0_epc};
    assign exception_wb_bus={inst_eret&pipeline_en,pre_exception_label&pipeline_en};
    assign exception_id_bus={inst_eret&pipeline_en,pre_exception_label&pipeline_en,cp0_status,cp0_cause};
endmodule