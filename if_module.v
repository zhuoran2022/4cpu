`include "define_bus.v"
module if_module (
    input wire clk,rst,
    //pipeline blocking
    input wire next_allowin,
    output wire this_valid,
    //bus
    input wire[`S1_BUS-1:0] pre_bus,
    output wire[`S2_BUS-1:0] next_bus,
    //inst
    input wire[31:0] inst_ram_rdata,
    output wire inst_ram_en,
    output wire[3:0] inst_ram_wen,
    output wire[31:0] inst_ram_addr,
    output wire[31:0] inst_ram_wdata,
    //exception
    input wire[33:0] exception_pc_bus
);

    //exception
    wire [31:0] epc_val;
    wire inst_eret,exception_happen;
    assign {inst_eret,exception_happen,epc_val}=exception_pc_bus;

    //pre_bus_parse
    wire is_br,stall,is_slot;
    wire[31:0] br_addr;
    assign {is_slot,stall,is_br,br_addr}=pre_bus;
    //about reg
    reg [31:0] pipe_reg;
    //pc parse
    wire [31:0] next_pc=exception_happen?32'hbfc00380:
                        inst_eret?epc_val:
                        is_br?br_addr:
                              pipe_reg+4;

    //pipeline blocking
    wire can_trans;
    wire is_exception=inst_eret|exception_happen;
    wire pipeline_en;
    blocking_module blocking1(
        .clk(clk),
        .rst(rst),
        .pre_valid(~rst&~stall),
        .this_valid(this_valid),
        .next_allowin(next_allowin),
        .can_trans(can_trans),
        .ready_go(1'b1),
        .is_exception(is_exception),
        .pipeline_en(pipeline_en)
    );
    always @(posedge clk) begin
        if(rst)begin
            pipe_reg<=32'hbfbffffc;
        end
        else if(can_trans)begin
            pipe_reg<=next_pc;
        end
    end
    //next_bus_parse
    wire[31:0] inst;
    //传给下一级看看这级的exception
    wire addr_error=(|pipe_reg[1:0])&pipeline_en;
    wire[1:0] exception_situation={addr_error,is_slot};
    assign next_bus={exception_situation,inst,pipe_reg};
    //about inst ram
    assign inst_ram_en=can_trans&~stall;
    assign inst_ram_wen=0;
    assign inst_ram_addr=next_pc;
    assign inst_ram_wdata=0;
    assign inst=inst_ram_rdata;
endmodule