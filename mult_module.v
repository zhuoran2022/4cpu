module mult_module (
    input[31:0] op1,op2,
    input inst_mult,inst_multu,
    output [63:0] mult_result
);
    //全换成正数算,省电路
    wire [32:0] mul_op1=(inst_mult&op1[31])?~{1'b1,op1}+1:{1'b0,op1};
    wire[32:0] mul_op2=(inst_mult&op2[31])?~{1'b1,op2}+1:{1'b0,op2};

    wire signal_diff=inst_mult&(op1[31]^op2[31]);
    
    wire[63:0] tmp_result=$signed(mul_op1)*$signed(mul_op2);
    assign mult_result=signal_diff?~tmp_result+1:tmp_result;
endmodule