`define SIGNAL_CONTROL_BUS 26

`define S1_BUS 35
`define S2_BUS 66
`define S3_BUS 142+`SIGNAL_CONTROL_BUS
`define S4_BUS 165
`define S5_BUS 124


`define RF_BUS 41

`define EX_HZ_BUS 41
`define OTHER_HZ_BUS 40

`define INT         5'h00 
`define EX_ADEL     5'h04
`define EX_ADES     5'h05
`define Ov          5'h0c
`define Sys         5'h08
`define Bp          5'h09
`define RI          5'h0a