module mycpu_top (
    input         clk,
    input         resetn,
    // inst sram interface
    output        inst_sram_en,
    output [ 3:0] inst_sram_wen,
    output [31:0] inst_sram_addr,
    output [31:0] inst_sram_wdata,
    input  [31:0] inst_sram_rdata,
    // data sram interface
    output        data_sram_en,
    output [ 3:0] data_sram_wen,
    output [31:0] data_sram_addr,
    output [31:0] data_sram_wdata,
    input  [31:0] data_sram_rdata,
    // trace debug interface
    output [31:0] debug_wb_pc,
    output [ 3:0] debug_wb_rf_wen,
    output [ 4:0] debug_wb_rf_wnum,
    output [31:0] debug_wb_rf_wdata,
    input [5:0] ext_int
);
    reg rst;
    always @(posedge clk) begin
        rst<=~resetn;
    end
    //blocking
   wire s2_allowin,s3_allowin,s4_allowin,s5_allowin;
   wire s1_valid,s2_valid,s3_valid,s4_valid;
    //bus
    wire [`S2_BUS-1:0] s2_bus;
    wire [`S1_BUS-1:0] br_bus;
    wire [`S3_BUS-1:0] s3_bus;
    wire [`S4_BUS-1:0] s4_bus;
    wire [`S5_BUS-1:0] s5_bus;
    wire [`RF_BUS-1:0] rf_bus;
    //hazard
    wire [`EX_HZ_BUS-1:0] ex_hz_bus;
    wire[`OTHER_HZ_BUS-1:0] mem_hz_bus,wb_hz_bus;
    //exception
    wire [33:0] exception_if_bus;
    wire [65:0] exception_id_bus;
    wire [1:0] exception_wb_bus;
    wire [1:0] exception_mem_bus;
    //s1
    if_module if_module(
        .clk(clk),
        .rst(rst),
        .next_allowin(s2_allowin),
        .this_valid(s1_valid),
        .pre_bus(br_bus),
        .next_bus(s2_bus),
        //inst ram
        .inst_ram_rdata(inst_sram_rdata),
        .inst_ram_en(inst_sram_en),
        .inst_ram_wen(inst_sram_wen),
        .inst_ram_addr(inst_sram_addr),
        .inst_ram_wdata(inst_sram_wdata),
        //exception
        .exception_pc_bus(exception_if_bus)
    );
    //s2
    id_module id_module(
        .clk(clk),
        .rst(rst),
        .pre_valid(s1_valid),
        .next_allowin(s3_allowin),
        .this_valid(s2_valid),
        .this_allowin(s2_allowin),
        .pre_bus(s2_bus),
        .next_bus(s3_bus),
        .br_bus(br_bus),
        .rf_bus(rf_bus),
        //hazard
        .ex_hz_bus(ex_hz_bus),
        .mem_hz_bus(mem_hz_bus),
        .wb_hz_bus(wb_hz_bus),
        //exception
        .cp0_info(exception_id_bus)
    );
    //s3
    exe_module exe_module(
        .clk(clk),
        .rst(rst),
        .pre_valid(s2_valid),
        .next_allowin(s4_allowin),
        .this_valid(s3_valid),
        .this_allowin(s3_allowin),
        .pre_bus(s3_bus),
        .next_bus(s4_bus),
        //data ram
        .data_ram_en(data_sram_en),
        .data_ram_wen(data_sram_wen),
        .data_ram_addr(data_sram_addr),
        .data_ram_wdata(data_sram_wdata),
        //hazard
        .ex_hz_bus(ex_hz_bus),
        .exception_wb_info(exception_wb_bus),
        .exception_mem_info(exception_mem_bus)
    );
    //s4
    mem_module mem_module(
        .clk(clk),
        .rst(rst),
        .pre_valid(s3_valid),
        .next_allowin(s5_allowin),
        .this_valid(s4_valid),
        .this_allowin(s4_allowin),
        .pre_bus(s4_bus),
        .next_bus(s5_bus),
        //data ram
        .data_ram_rdata(data_sram_rdata),
        //hazard
        .mem_hz_bus(mem_hz_bus),
        .exception_wb_info(exception_wb_bus),
        .exception_mem_info(exception_mem_bus)
    );
    //s5
    wb_module wb_module(
        .clk(clk),
        .rst(rst),
        .pre_valid(s4_valid),
        .this_allowin(s5_allowin),
        .pre_bus(s5_bus),
        .rf_bus(rf_bus),
        .debug_wb_pc(debug_wb_pc),
        .debug_wb_rf_wen(debug_wb_rf_wen),
        .debug_wb_rf_wnum(debug_wb_rf_wnum),
        .debug_wb_rf_wdata(debug_wb_rf_wdata),
        .wb_hz_bus(wb_hz_bus),
        .exception_if_bus(exception_if_bus),
        .exception_id_bus(exception_id_bus),
        .exception_wb_bus(exception_wb_bus),
        .ext_int(ext_int)
    );
endmodule