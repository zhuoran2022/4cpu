`include "define_bus.v"
module mem_module (
    input wire clk,rst,
    //blocking pipeline
    input wire pre_valid,next_allowin,
    output wire this_valid,this_allowin,
    //bus
    input wire[`S4_BUS-1:0] pre_bus,
    output wire[`S5_BUS-1:0] next_bus,
    //data_ram
    input wire[31:0] data_ram_rdata,
    //hazard
    output wire[`OTHER_HZ_BUS-1:0] mem_hz_bus,
    //exception
    input wire[1:0] exception_wb_info,
    output wire[1:0] exception_mem_info
);
    //exception parse
    wire wb_exception_happen,wb_eret;
    wire mem_exception_happen,mem_eret;
    assign {wb_eret,wb_exception_happen}=exception_wb_info;
    assign exception_mem_info={mem_eret,mem_exception_happen};
    //pipeline blocking
    wire can_trans,pipeline_en;
    blocking_module blocking4(
        .clk(clk),
        .rst(rst),
        .pre_valid(pre_valid),
        .this_valid(this_valid),
        .next_allowin(next_allowin),
        .this_allowin(this_allowin),
        .can_trans(can_trans),
        .pipeline_en(pipeline_en),
        .ready_go(1'b1),
        .is_exception(wb_exception_happen|wb_eret)
    );
    //reg and parse
    reg[`S4_BUS-1:0] pipe_reg;
    wire mem_to_reg,reg_write;
    wire[4:0] dest;
    wire [31:0] alu_result,pc;
    wire [3:0] load_control_bus;
    wire inst_lb,inst_lbu,inst_lh,inst_lhu;
    assign {inst_lb,inst_lbu,inst_lh,inst_lhu}=load_control_bus;

    wire [1:0] unalignment_load;
    wire inst_lwl,inst_lwr;
    assign {inst_lwl,inst_lwr}=unalignment_load;
    wire [50:0] pre_exception_situation;
    wire data_ram_en;
    wire[3:0] data_ram_wen;
    wire [31:0] data_ram_wdata;
    wire [36:0] cache_need_bus={data_ram_en,data_ram_wen,data_ram_wdata};
    assign {cache_need_bus,
            pre_exception_situation,
            unalignment_load,
            load_control_bus,
            mem_to_reg,
            reg_write,
            dest,
            alu_result,
            pc}=pipe_reg;
    always @(posedge clk) begin
        if(can_trans)begin
            pipe_reg<=pre_bus;
        end
    end
    
    //lb,lbu,lh,lhu
    wire [1:0] load_addr=alu_result[1:0];
    //始终取低位
    wire[31:0] load_word=data_ram_rdata;
    wire[15:0] load_half_word=load_addr[1]?load_word[31:16]:load_word[15:0];
    wire[7:0] load_byte=load_addr[0]?load_half_word[15:8]:load_half_word[7:0];
    wire[31:0] lb_lbu_result={{24{inst_lb&load_byte[7]}},load_byte};
    wire[31:0] lh_lhu_result={{16{inst_lh&load_half_word[15]}},load_half_word};

    //lwl lwr
    wire [31:0] lwl_word=(load_addr==2'b00)?{load_word[7:0],24'b0}:
                         (load_addr==2'b01)?{load_word[15:0],16'b0}:
                         (load_addr==2'b10)?{load_word[23:0],8'b0}:
                                            load_word[31:0];
    wire [31:0] lwr_word=(load_addr==2'b11)?{24'b0,load_word[31:24]}:
                         (load_addr==2'b10)?{16'b0,load_word[31:16]}:
                         (load_addr==2'b01)?{8'b0,load_word[31:8]}:
                                            load_word[31:0];

    wire[3:0] lwl_wen_bit=(load_addr==2'b00)?4'b1000:
                          (load_addr==2'b01)?4'b1100:
                          (load_addr==2'b10)?4'b1110:
                                             4'b1111;
    wire[3:0] lwr_wen_bit=(load_addr==2'b00)?4'b1111:
                          (load_addr==2'b01)?4'b0111:
                          (load_addr==2'b10)?4'b0011:
                                             4'b0001;
    
    wire[3:0] final_wen_bit=inst_lwl?lwl_wen_bit:
                            inst_lwr?lwr_wen_bit:
                                     {4{reg_write}};

    wire [31:0] final_load_result=(inst_lb|inst_lbu)?lb_lbu_result:
                                  (inst_lh|inst_lhu)?lh_lhu_result:
                                  (inst_lwl)?        lwl_word:
                                  (inst_lwr)?        lwr_word:
                                                     load_word;

    wire[31:0] final_result=mem_to_reg?final_load_result:alu_result;
    //TODO 明天继续扩线路，寄存器的

    //next bus
    assign next_bus={
        pre_exception_situation,final_wen_bit,dest,final_result,pc
    };

    //hazard
    assign mem_hz_bus={pre_exception_situation[3]&pipeline_en,pipeline_en,reg_write,dest,final_result};
    assign mem_exception_happen=pre_exception_situation[50]&pipeline_en;
    assign mem_eret=pre_exception_situation[1]&pipeline_en;
endmodule