`define special 6'b000_000
`define reg_imm 6'b000_001
`define cop0_inst 6'b010_000
//R type
`define ADDU 6'h21
`define SUBU 6'h23
`define SLT 6'h2a
`define SLTU 6'h2b
`define AND 6'h24
`define OR 6'h25
`define XOR 6'h26
`define NOR 6'h27
`define SLL 6'h0
`define SRL 6'h2
`define SRA 6'h3
`define JR 6'h08
`define ADD 6'h20
`define SUB 6'h22
`define SLLV 6'h04
`define SRAV 6'h07
`define SRLV 6'h06
`define MULT 6'h18
`define MULTU 6'h19
`define DIV 6'h1a
`define DIVU 6'h1b
`define MFHI 6'h10
`define MTHI 6'h11
`define MFLO 6'h12
`define MTLO 6'h13
//I type
`define ADDI 6'h08
`define ADDIU 6'h09
`define LUI 6'h0f
`define LW 6'h23
`define SW 6'h2b
`define BEQ 6'h04
`define BNE 6'h05
`define JAL 6'h03
`define SLTI 6'h0a
`define SLTIU 6'h0b
`define ANDI 6'h0c
`define ORI 6'h0d
`define XORI 6'h0e
//other branch
`define BGEZ 5'h01

`define BGTZ 6'h07
`define BLEZ 6'h06

`define BLTZ 5'h00
`define J 6'h02
`define BLTZAL 5'h10
`define BGEZAL 6'h11
`define JALR 6'h09
//other store load
`define LB 6'h20
`define LBU 6'h24
`define LH 6'h21
`define LHU 6'h25
`define LWL 6'h22
`define LWR 6'h26
`define SB 6'h28
`define SH 6'h29
`define SWL 6'h2a
`define SWR 6'h2e
//epc
`define BREAK 6'h0d
`define SYSCALL 6'h0c
`define MFC0 5'h00
`define MTC0 5'h04
`define ERET 6'h18
`include "define_bus.v"


module decoder_module (
    input wire[31:0] inst,
    input wire[31:0] pc,
    input wire[31:0] rf_data1,rf_data2,
    output wire[`SIGNAL_CONTROL_BUS-1:0] control_bus,
    output wire[11:0] alu_op,
    output wire[4:0] dest,
    //about branch
    output wire is_br,
    output wire [31:0] br_addr,
    output wire is_slot,
    output wire[6:0] exception_parse_bus
);
    wire[5:0] opcode,funct;
    wire[4:0] rs,rt,rd,shamt;
    wire[25:0] target;
    wire[15:0] imm;
    assign {opcode,rs,rt,rd,shamt,funct}=inst;
    assign target=inst[25:0];
    assign imm=inst[15:0];

    //decode
    wire [63:0] opcode_d,funct_d;

    wire [31:0] rs_d,rt_d,rd_d,shamt_d;
    XtoYdecode#(
        .IN_WIDTH(5),
        .OUT_WIDTH(32))
        decode_5to32_rs(
            .in(rs),
            .out(rs_d)
        );
    XtoYdecode#(
        .IN_WIDTH(5),
        .OUT_WIDTH(32))
        decode_5to32_rt(
            .in(rt),
            .out(rt_d)
        );
    XtoYdecode#(
        .IN_WIDTH(5),
        .OUT_WIDTH(32))
        decode_5to32_rd(
            .in(rd),
            .out(rd_d)
        );
    XtoYdecode#(
        .IN_WIDTH(5),
        .OUT_WIDTH(32))
        decode_5to32_shamt(
            .in(shamt),
            .out(shamt_d)
        );

    XtoYdecode#(
        .IN_WIDTH(6),
        .OUT_WIDTH(64))
        decode_6to64_opcode(
            .in(opcode),
            .out(opcode_d)
        );
    XtoYdecode#(
        .IN_WIDTH(6),
        .OUT_WIDTH(64))
        decode_6to64_funct(
            .in(funct),
            .out(funct_d)
        );

    wire is_special=opcode_d[`special];
    wire is_reg_imm=opcode_d[`reg_imm];
    wire is_cop0_inst=opcode_d[`cop0_inst];
    //R type
    wire inst_addu=funct_d[`ADDU]&is_special;
    wire inst_subu=funct_d[`SUBU]&is_special;
    wire inst_slt=funct_d[`SLT]&is_special;
    wire inst_sltu=funct_d[`SLTU]&is_special;
    wire inst_and=funct_d[`AND]&is_special;
    wire inst_or=funct_d[`OR]&is_special;
    wire inst_xor=funct_d[`XOR]&is_special;
    wire inst_nor=funct_d[`NOR]&is_special;
    wire inst_sll=funct_d[`SLL]&is_special;
    wire inst_srl=funct_d[`SRL]&is_special;
    wire inst_sra=funct_d[`SRA]&is_special;
    wire inst_jr=funct_d[`JR]&is_special;
    wire inst_add=funct_d[`ADD]&is_special;
    wire inst_sub=funct_d[`SUB]&is_special;
    wire inst_sllv=funct_d[`SLLV]&is_special;
    wire inst_srav=funct_d[`SRAV]&is_special;
    wire inst_srlv=funct_d[`SRLV]&is_special;
    wire inst_mult=funct_d[`MULT]&is_special;
    wire inst_multu=funct_d[`MULTU]&is_special;
    wire inst_div=funct_d[`DIV]&is_special;
    wire inst_divu=funct_d[`DIVU]&is_special;
    wire inst_mfhi=funct_d[`MFHI]&is_special;
    wire inst_mflo=funct_d[`MFLO]&is_special;
    wire inst_mthi=funct_d[`MTHI]&is_special;
    wire inst_mtlo=funct_d[`MTLO]&is_special;

    wire inst_bgez=is_reg_imm&rt_d[`BGEZ];
    wire inst_bgtz=opcode_d[`BGTZ];
    wire inst_blez=opcode_d[`BLEZ];
    wire inst_bltz=is_reg_imm&rt_d[`BLTZ];
    wire inst_j=opcode_d[`J];
    wire inst_bltzal=is_reg_imm&rt_d[`BLTZAL];
    wire inst_bgezal=is_reg_imm&rt_d[`BGEZAL];
    wire inst_jalr=is_special&funct_d[`JALR];
    //I type
    wire inst_addiu=opcode_d[`ADDIU];
    wire inst_lui=opcode_d[`LUI];
    wire inst_lw=opcode_d[`LW];
    wire inst_sw=opcode_d[`SW];
    wire inst_beq=opcode_d[`BEQ];
    wire inst_bne=opcode_d[`BNE];
    wire inst_addi=opcode_d[`ADDI];
    wire inst_slti=opcode_d[`SLTI];
    wire inst_sltiu=opcode_d[`SLTIU];
    wire inst_andi=opcode_d[`ANDI];
    wire inst_ori=opcode_d[`ORI];
    wire inst_xori=opcode_d[`XORI];
    wire inst_lb=opcode_d[`LB];
    wire inst_lbu=opcode_d[`LBU];
    wire inst_lh=opcode_d[`LH];
    wire inst_lhu=opcode_d[`LHU];
    wire inst_lwl=opcode_d[`LWL];
    wire inst_lwr=opcode_d[`LWR];
    wire inst_sb=opcode_d[`SB];
    wire inst_sh=opcode_d[`SH];
    wire inst_swl=opcode_d[`SWL];
    wire inst_swr=opcode_d[`SWR];
    //J type
    wire inst_jal=opcode_d[`JAL];
    //exception
    wire inst_mfc0=is_cop0_inst&rs_d[`MFC0];
    wire inst_mtc0=is_cop0_inst&rs_d[`MTC0];
    wire inst_eret=is_cop0_inst&funct_d[`ERET];
    wire inst_syscall=is_special&funct_d[`SYSCALL];
    wire inst_break=is_special&funct_d[`BREAK];
    //about br
    wire rs_eq_rt=(rf_data1==rf_data2);
    //大于等于0分支
    wire rs_bgez_imm=~rf_data1[31];
    //小于0分支
    wire rs_bltz_imm=rf_data1[31];
    //大于0分支
    wire rs_bgtz_imm=~rf_data1[31]&(rf_data1!=0);
    //小于等于0分支
    wire rs_blez_imm=~rs_bgtz_imm;
    assign is_br=inst_beq&rs_eq_rt|inst_bne&~rs_eq_rt|inst_jal|inst_jr|inst_j|
                 inst_bgez&rs_bgez_imm|inst_bltz&rs_bltz_imm|
                 inst_blez&rs_blez_imm|inst_bgtz&rs_bgtz_imm|
                 inst_bgezal&rs_bgez_imm|inst_bltzal&rs_bltz_imm|
                 inst_jalr;

    assign is_slot=inst_beq|inst_bne|inst_jal|inst_jr|inst_j|
                   inst_bgez|inst_bltz|inst_blez|inst_bgtz|
                   inst_bgezal|inst_bltzal|inst_jalr; 

    assign br_addr=(inst_beq|inst_bne|
                    inst_bgez|inst_bltz|inst_bgtz|inst_blez|inst_bltzal|inst_bgezal
                    )?(pc+{{14{imm[15]}},imm[15:0],2'b0}):
                    inst_jr|inst_jalr?rf_data1:
                    //jal,j
                    {pc[31:28],target[25:0],2'b0};
//**********************signal*********************//
//一共有9个
    //signal about op
    wire op1_id_shamt=inst_sll|inst_srl|inst_sra;
    wire op1_is_pc=inst_jal|inst_bgezal|inst_bltzal|inst_jalr;
    wire op2_imm_zero_ext=inst_andi|inst_ori|inst_xori;
    wire op2_is_imm=inst_addiu|inst_lui|inst_lw|inst_sw|inst_addi|inst_slti|inst_sltiu|op2_imm_zero_ext|
                    inst_lb|inst_lbu|inst_lh|inst_lhu|
                    inst_sb|inst_sh|
                    inst_lwl|inst_lwr|
                    inst_swl|inst_swr;
    wire op2_is_8=inst_jal|inst_bgezal|inst_bltzal|inst_jalr;

    //signal about sw,lw
    wire mem_to_reg=inst_lw|inst_lb|inst_lbu|inst_lh|inst_lhu|
                    inst_lwr|inst_lwl;
    wire mem_we=inst_sw|inst_sb|inst_sh|inst_swl|inst_swr;
    wire reg_write=inst_addu|inst_subu|inst_slt|inst_sltu|inst_and|inst_or|
                    inst_xor|inst_nor|inst_sll|inst_srl|inst_sra|inst_addiu|
                    inst_lw|inst_jr|inst_lui|inst_jal|inst_add|inst_sub|inst_slti|inst_sltiu|inst_addi|
                    inst_andi|inst_ori|inst_xori|inst_sllv|inst_srlv|inst_srav|
                    inst_mflo|inst_mfhi|inst_bgezal|inst_bltzal|inst_jalr|
                    inst_lb|inst_lbu|inst_lh|inst_lhu|
                    inst_lwl|inst_lwr|
                    inst_mfc0;
    //signal about rf dst
    wire dst_is_r31=inst_jal|inst_bgezal|inst_bltzal;
    wire dst_is_rt=inst_addiu|inst_lui|inst_lw|inst_addi|inst_slti|inst_sltiu|inst_andi|inst_ori|inst_xori|
                   inst_lb|inst_lbu|inst_lh|inst_lhu|
                   inst_lwl|inst_lwr|
                   inst_mfc0;
    //other control
    wire[1:0] unalignment_load={inst_lwl,inst_lwr};
    wire [3:0] mul_div_bus={inst_mult,inst_multu,inst_div,inst_divu};
    wire [3:0] hi_lo_control_bus={inst_mthi,inst_mtlo,inst_mfhi,inst_mflo};
    wire [3:0] load_control_bus={inst_lb,inst_lbu,inst_lh,inst_lhu};
    wire [3:0] store_control_bus={inst_sb,inst_sh,inst_swl,inst_swr};
    //alu op
    //+
    assign alu_op[0]=inst_addu|inst_addiu|inst_lw|inst_sw|inst_jal|inst_add|inst_addi|
                     inst_bltzal|inst_bgezal|inst_jalr|
                     inst_lb|inst_lbu|inst_lh|inst_lhu|
                     inst_sb|inst_sh|
                     inst_lwl|inst_lwr|
                     inst_swl|inst_swr;
    //-
    assign alu_op[1]=inst_subu|inst_sub;
    //slt
    assign alu_op[2]=inst_slt|inst_slti;
    //sltu
    assign alu_op[3]=inst_sltu|inst_sltiu;
    //and
    assign alu_op[4]=inst_and|inst_andi;
    //nor
    assign alu_op[5]=inst_nor;
    //or
    assign alu_op[6]=inst_or|inst_ori;
    //xor
    assign alu_op[7]=inst_xor|inst_xori;
    //sll
    assign alu_op[8]=inst_sll|inst_sllv;
    //srl
    assign alu_op[9]=inst_srl|inst_srlv;
    //sra
    assign alu_op[10]=inst_sra|inst_srav;
    //lui
    assign alu_op[11]=inst_lui;


    //写寄存器的地址
    assign dest=dst_is_r31?5'd31:dst_is_rt?rt:rd;


    //about exception
    wire unreserved_inst=~(inst_addu | inst_subu | inst_slt | inst_sltu | inst_and | inst_or | inst_xor | inst_nor
    | inst_sll | inst_srl | inst_sra | inst_addiu | inst_lui | inst_lw | inst_sw | inst_beq | inst_bne | inst_jal
    | inst_jr | inst_add | inst_addi | inst_sub | inst_slti | inst_sltiu | inst_andi | inst_ori | inst_xori | inst_sllv
    | inst_srlv | inst_srav | inst_mult | inst_multu | inst_div | inst_divu | inst_mfhi | inst_mflo | inst_mthi | inst_mtlo
    | inst_bgez | inst_bgtz | inst_blez | inst_bltz | inst_j | inst_bltzal | inst_bgezal | inst_jalr | inst_lb | inst_lbu
    | inst_lh | inst_lhu | inst_lwl | inst_lwr | inst_sb | inst_sh | inst_swl | inst_swr | inst_syscall | inst_eret | inst_mfc0
    | inst_mtc0 | inst_break);
    wire overflow_inst=inst_add|inst_sub|inst_addi;
    assign exception_parse_bus={unreserved_inst,overflow_inst,inst_mfc0,inst_mtc0,inst_eret,inst_syscall,
                                inst_break};


    assign control_bus={op1_id_shamt,op1_is_pc,op2_imm_zero_ext,op2_is_imm,op2_is_8,
                        mem_to_reg,mem_we,reg_write,mul_div_bus,hi_lo_control_bus,
                        load_control_bus,store_control_bus,unalignment_load};
endmodule