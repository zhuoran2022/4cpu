module XtoYdecode #(
    parameter IN_WIDTH=3,
    parameter OUT_WIDTH=8
) (
    input wire[IN_WIDTH-1:0] in,
    output wire[OUT_WIDTH-1:0] out
);
    genvar i;
    generate
        for (i = 0; i<OUT_WIDTH; i=i+1) begin:de
            assign out[i]=(in==i);
        end
    endgenerate
    
endmodule