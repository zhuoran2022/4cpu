module regfile (
    input wire clk,
    //input port
    input wire[4:0] raddr1,raddr2,waddr,
    input wire[31:0] wdata,
    input wire[3:0] we,
    //output port
    output wire[31:0] rdata1,rdata2
);
  reg [31:0] regs[31:0];
  always @(posedge clk) begin
    if(|we)begin
        regs[waddr][7:0]<=we[0]?wdata[7:0]:regs[waddr][7:0];
        regs[waddr][15:8]<=we[1]?wdata[15:8]:regs[waddr][15:8];
        regs[waddr][23:16]<=we[2]?wdata[23:16]:regs[waddr][23:16];
        regs[waddr][31:24]<=we[3]?wdata[31:24]:regs[waddr][31:24];
    end
  end  
  assign rdata1=(raddr1==0)?32'b0:regs[raddr1];
  assign rdata2=(raddr2==0)?32'b0:regs[raddr2];
endmodule